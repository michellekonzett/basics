package car;

public class engine
{
	private String fuelType;
	private double Power;
	
	public engine(String fuelType, double power)
	{
		super();
		this.fuelType = fuelType;
		Power = power;
	}
	
	public String getFuelType()
	{
		return fuelType;
	}
	
	public double getPower()
	{
		return Power;
	}
	
}