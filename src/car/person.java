package car;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class person {
	private List<car> cars;
	private String firstName;
	private String lastName;
	private LocalDate birthDate;
	
	public person(String firstName, String lastName, LocalDate birthDate)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		
		cars = new ArrayList<car>();
	}
	
	public List<car> getCars()
	{
		return cars;
	}

	public void addCar(car car)
	{
		cars.add(car);
	}
	
	public double getValueOfCars()
	{
		double value = 0;
		
		for(car car : cars)
		{
			value += car.getPrice();
		}
		
		return value;
	}
	
	public int getAge()
	{
		return Period.between(this.birthDate, LocalDate.now()).getYears();
	}
}