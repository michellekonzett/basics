package car;

import java.time.LocalDate;
import java.util.Date;

public class main {

	public static void main(String[] args)
	{
		engine e1 = new engine("Diesel", 200);
		manufacturer m1 = new manufacturer("VW","Germany", 15);
		car c1 = new car("Black", 150, 10000, 0.09, e1, m1, 49000);
		
		engine e2 = new engine("Benzin", 300);
		manufacturer m2 = new manufacturer("BMW","Germany", 10);
		car c2 = new car("Red", 180, 15000, 0.07, e2, m2, 150000);
		
		engine e3 = new engine("Benzin", 300);
		manufacturer m3 = new manufacturer("Alpha Romeo","Italy", 5);
		car c3 = new car("Orange", 200, 20000, 0.1, e3, m3, 50000);
		
		engine e4 = new engine("Diesel", 200);
		manufacturer m4 = new manufacturer("Mazda","Japan", 15);
		car c4 = new car("Withe", 150, 8000, 0.08, e4, m4, 120000);
		
		person p1 = new person("Michelle", "Konzett", LocalDate.parse("2000-11-20"));
		
		person p2 = new person("Jonas", "Both", LocalDate.parse("2000-12-18"));
		
		Date today = new Date();
				
		p1.addCar(c1);
		p1.addCar(c2);		
		p2.addCar(c3);
		
		
		for(car car : p1.getCars())
		{
			System.out.println(car.getUsage());
			System.out.println(car.getType());
			System.out.println(car.getPrice());
		}
		
		System.out.println(p1.getValueOfCars());
		System.out.println(p1.getAge());		
	}
}