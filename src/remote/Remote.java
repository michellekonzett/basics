package remote;
import remote.Battery;

public class Remote
{
	private boolean isOn=false;
	private Battery battery;
	
	public Remote(Battery battery)
	{
		this.battery=battery;
	}
	
	//turnOn
	public void turnOn()
	{
		this.isOn=true;
		System.out.println("Now I'm turned on.");
	}
	
	//turnOff
	public void turnOff()
	{
		this.isOn=false;
		System.out.println("Now I'm turned off.");
	}
	
	//isOn
	public boolean isOn()
	{
		return this.isOn;
	}
	
	//getStatus
	public boolean hasPower()
	{
		if(battery.getChargingStatus()>50)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
}
