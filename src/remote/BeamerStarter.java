package remote;
import remote.Battery;

public class BeamerStarter {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		Battery b1 = new Battery();
		Battery b2 = new Battery();
		
		Remote r1 = new Remote(b1);
		Remote r2 = new Remote(b2);
		b2.setChargingStatus(40);
		
		r1.turnOn();
		
		System.out.println(r1.hasPower());
		System.out.println(r1.isOn());
		
		r2.turnOn();
		System.out.println(r2.hasPower());
		System.out.println(r2.isOn());
		r2.turnOff();
	}

}
