
public class Rectangle 
{
	//Gedächtnisvariable, Instanz
	private int a, b;
	
	public Rectangle(int a, int b)
	{
		this.a = a;
		this.b = b;
	}
	
	//a zurückliefern
	public int getA()
	{
		return this.a;
	}
	
	//b zurückliefern
	public int getB()
	{
		return this.b;
	}
	
	//a setzen
	public void setA(int a)
	{
		this.a = b;
	}
	
	//b setzen
	public void setB(int b)
	{
		this.b = a;
	}
	
	public void sayHallo()
	{
		System.out.println("Ich bin "+ this.a + " breit und "+ this.b + " lang.");
	}
	
	//get Area
	public double getArea()
	{
		int Area = 0;
		
		Area = a*b;
		return Area;
	}
	
	//getCircumference
	
	public double getCircumference()
	{
		int Circumference = 0;
		
		Circumference = 2*a + 2*b;
		return Circumference;
	}
	
}
